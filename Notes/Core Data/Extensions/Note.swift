//
//  File.swift
//  Notes
//
//  Created by Carolina Arcos on 19/07/21.
//

import Foundation

extension Note {
    var updatedAtAsDate: Date {
        return updatedAt ?? Date()
    }
    
    var createdAtAsDate: Date {
        return createdAt ?? Date()
    }
}
