//
//  Date.swift
//  Notes
//
//  Created by Carolina Arcos on 19/07/21.
//

import Foundation

extension Date {
    func formattedAsString() -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "MMM d, HH:mm"
        return formatter.string(from: self)
    }
}
