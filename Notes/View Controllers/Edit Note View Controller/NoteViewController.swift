//
//  NoteViewController.swift
//  Notes
//
//  Created by Carolina Arcos on 19/07/21.
//

import UIKit

class NoteViewController: UIViewController {
    
    // MAR: - Outlets
    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var contentsTextView: UITextView!
    
    // MARK: - Properties
    var note: Note?
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }
    
    // MARK: - Actions
    @IBAction func update(_ sender: Any) {
        guard let note = note else { return }
        
        guard let title = titleTextField.text, !title.isEmpty else {
            showAlert(with: "Title Missing", and: "Your note requires a title.")
            return
        }
        
        if title != note.title {
            note.title = title
        }
        
        if contentsTextView.text != note.contents {
            note.contents = contentsTextView.text
        }
        
        if note.isUpdated {
            note.updatedAt = Date()
        }
        
        navigationController?.popViewController(animated: true)
    }
}

private extension NoteViewController {
    func setupView() {
        title = "Edit Note"
        titleTextField.text = note?.title
        contentsTextView.text = note?.contents
    }
}
