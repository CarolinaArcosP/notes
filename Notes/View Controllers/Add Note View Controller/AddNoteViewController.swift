//
//  AddNoteViewController.swift
//  Notes
//
//  Created by Carolina Arcos on 16/07/21.
//

import UIKit
import CoreData

class AddNoteViewController: UIViewController {
    
    // MAR: - Outlets
    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var contentsTextView: UITextView!
    
    // MARK: - Properties
    var managedObjectContext: NSManagedObjectContext!
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Add Note"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        titleTextField.becomeFirstResponder()
    }
    
    // MARK: - Actions
    @IBAction func save(_ sender: Any) {
        guard let title = titleTextField.text, !title.isEmpty else {
            showAlert(with: "Title Missing", and: "Your note requires a title.")
            return
        }
        
        let note = Note(context: managedObjectContext)
        note.title = title
        note.createdAt = Date()
        note.updatedAt = Date()
        note.contents = contentsTextView.text
        
        navigationController?.popViewController(animated: true)
    }
    
}
