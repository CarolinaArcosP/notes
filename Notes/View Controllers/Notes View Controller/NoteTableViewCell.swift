//
//  NoteTableViewCell.swift
//  Notes
//
//  Created by Carolina Arcos on 19/07/21.
//

import UIKit

class NoteTableViewCell: UITableViewCell {
    
    // MARK: - Static properties
    static let reuseIdentifier = "NoteTableViewCell"
    
    // MARK: - Outlets
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var contentsLabel: UILabel!
    @IBOutlet weak var updatedAtLabel: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }
}
