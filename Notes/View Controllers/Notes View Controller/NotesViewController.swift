//
//  NotesViewController.swift
//  Notes
//
//  Created by Carolina Arcos on 16/07/21.
//

import UIKit
import CoreData

class NotesViewController: UIViewController {
    // MARK: Segues
    private enum Segue {
        static let AddNote = "AddNote"
        static let EditNote = "EditNote"
    }
    
    // MARK: - Outlets
    @IBOutlet weak var notesView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var messageLabel: UILabel! {
        didSet {
            messageLabel.text = "You don't have any notes yet."
        }
    }
    
    // MARK: - Properties
    private let coreDataManager: CoreDataManager = CoreDataManager(modelName: "Notes")
    var notes: [Note]? {
        didSet {
            updateView()
        }
    }
    var hasNotes: Bool {
        return !(notes?.isEmpty ?? true)
    }

    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureTableView()
        fetchNotes()
        setupNotificationHandling()
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let identifier = segue.identifier else { return }
        switch identifier {
        case Segue.AddNote:
            guard let destionation = segue.destination as? AddNoteViewController else { return }
            destionation.managedObjectContext = coreDataManager.managedObjectContext
        case Segue.EditNote:
            guard let destionation = segue.destination as? NoteViewController,
                  let indexPath = tableView.indexPathForSelectedRow,
                  let note = notes?[indexPath.row] else { return }
            destionation.note = note
        default:
            break
        }
    }
}

// MARK: - Configuration methods
private extension NotesViewController {
    func configureTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.estimatedRowHeight = 66
    }
    
    func updateView() {
        tableView.isHidden = !hasNotes
        messageLabel.isHidden = hasNotes
    }
    
    func setupNotificationHandling() {
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self,
                                       selector: #selector(managedObjectContextObjectsDidchange(_:)),
                                       name: Notification.Name.NSManagedObjectContextObjectsDidChange,
                                       object: coreDataManager.managedObjectContext)
    }
}

extension NotesViewController {
    func fetchNotes() {
        let fetchRequest: NSFetchRequest<Note> = Note.fetchRequest()
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: #keyPath(Note.updatedAt), ascending: false)]
        
        /* performAndWait: access the managedObjectContext on the thread it's associated with
         also, it's executed sync -> blockes the thread it's executed */
        coreDataManager.managedObjectContext.performAndWait {
            do {
                notes = try fetchRequest.execute()
                tableView.reloadData()
            } catch {
                print("Unable to Execute Fetch Request")
                print("Error: \(error), \(error.localizedDescription)")
            }
        }
    }
    
    @objc func managedObjectContextObjectsDidchange(_ notification: Notification) {
        guard let userInfo = notification.userInfo else { return }
        
        var notesDidChannge = false
        if let inserts = userInfo[NSInsertedObjectsKey] as? Set<NSManagedObject> {
            let insertsNotes = inserts.compactMap { $0 as? Note }
            insertsNotes.forEach { notes?.append($0) }
            notesDidChannge = notesDidChannge || !insertsNotes.isEmpty
        }
        
        if let updates = userInfo[NSUpdatedObjectsKey] as? Set<NSManagedObject> {
            let updatesNotes = updates.compactMap { $0 as? Note }
            notesDidChannge = notesDidChannge || !updatesNotes.isEmpty
        }
        
        if let deletes = userInfo[NSDeletedObjectsKey] as? Set<NSManagedObject> {
            let deletesNotes = deletes.compactMap { $0 as? Note }
            for delete in deletesNotes {
                if let index = notes?.firstIndex(of: delete) {
                    notes?.remove(at: index)
                    notesDidChannge = true
                }
            }
        }
        
        
        if notesDidChannge {
            notes?.sort(by: { $0.updatedAtAsDate > $1.updatedAtAsDate })
            tableView.reloadData()
            updateView()
        }
    }
}

// MARK: - Table view delegate & data source
extension NotesViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notes?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let note = notes?[indexPath.row] else { fatalError("Unexpected Index Path") }
        let cell = tableView.dequeueReusableCell(withIdentifier: NoteTableViewCell.reuseIdentifier, for: indexPath) as! NoteTableViewCell
        cell.titleLabel.text = note.title
        cell.contentsLabel.text = note.contents
        cell.updatedAtLabel.text = note.updatedAtAsDate.formattedAsString()
        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        guard editingStyle == .delete else { return }
        guard let note = notes?[indexPath.row] else { fatalError("Unexpected Index Path") }
        
        coreDataManager.managedObjectContext.delete(note)
    }
}

